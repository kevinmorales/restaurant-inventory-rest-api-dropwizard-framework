package com.kmorale4;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.kmorale4.api.User;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

public class AuthMapper implements RowMapper<User> {

    @Override
    public User map(ResultSet rs, StatementContext ctx) throws SQLException {

        return new User(rs.getInt("id"), rs.getString("username"), rs.getBytes("salt"), rs.getBytes("user_password"),
                rs.getString("user_role"));
    }

}
