package com.kmorale4;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.kmorale4.api.Product;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

public class InventoryMapper implements RowMapper<Product> {

    @Override
    public Product map(ResultSet rs, StatementContext ctx) throws SQLException {

        return new Product(rs.getInt("id"), rs.getString("name"), rs.getString("unit"), rs.getDouble("unit_cost"),
                rs.getDouble("amount"), rs.getDouble("total_cost"));
    }
}
