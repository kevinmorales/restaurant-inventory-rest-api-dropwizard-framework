package com.kmorale4.db;

import java.util.List;
import com.kmorale4.api.Product;

import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

public interface InventoryDAO {

        // SELECT ALL
        @SqlQuery("select id, name, unit, unit_cost, amount, total_cost from product")
        public List<Product> getAll();

        // SELECT BY MONTH
        @SqlQuery("select id, name, unit, unit_cost, amount, total_cost from product where (month = :month)")
        public List<Product> getByMonth(@Bind("month") String month);

        // SELECT last id inserted
        @SqlQuery("SELECT LAST_INSERT_ID();")
        public int getLastId();

        // SELECT BY MONTH AND ID
        @SqlQuery("select id, name, unit, unit_cost, amount, total_cost from product where (id = :id) and (month = :month)")
        public Product getByMonthAndId(@Bind("id") Integer id, @Bind("month") String month);

        @SqlUpdate("insert into product (name, unit, unit_cost, amount, total_cost, month) values (:name, :unit, :unitCost, :amount, :totalCost, :month)")
        public void addToDb(@Bind("name") String name, @Bind("unit") String unit, @Bind("unitCost") Double unitCost,
                        @Bind("amount") Double amount, @Bind("totalCost") Double totalCost,
                        @Bind("month") String month);

        // DELETE BY ID
        @SqlUpdate("delete from product where (id = :id) and (month = :month)")
        public void deleteInventory(@Bind("id") Integer id, @Bind("month") String month);

        // UPDATE BY ID
        @SqlUpdate("update product as c set name = :name, unit = :unit, unit_cost = :unitCost, amount = :amount, total_cost = :totalCost, month = :month where (id = :id)")
        public void updateInventory(@Bind("id") Integer id, @Bind("name") String name, @Bind("unit") String unit,
                        @Bind("unitCost") Double unitCost, @Bind("amount") Double amount,
                        @Bind("totalCost") Double totalCost, @Bind("month") String month);

}