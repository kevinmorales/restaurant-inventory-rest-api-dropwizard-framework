package com.kmorale4.db;

import com.kmorale4.api.User;

import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

public interface AuthDAO {

    // SELECT ALL
    @SqlQuery("select * from user_info where (username = :username)")
    public User getUser(@Bind("username") String username);

    @SqlUpdate("insert into user_info (username, salt, user_password, user_role) values (:username, :salt, :user_password, :role)")
    public void addToDb(@Bind("username") String name, @Bind("salt") byte[] salt,
            @Bind("user_password") byte[] password, @Bind("role") String role);

    // SELECT last id inserted
    @SqlQuery("SELECT LAST_INSERT_ID();")
    public int getLastId();

}
