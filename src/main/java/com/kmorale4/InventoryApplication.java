package com.kmorale4;

import com.kmorale4.health.TemplateHealthCheck;
import com.kmorale4.resources.AuthResource;
import com.kmorale4.resources.InventoryById;
import com.kmorale4.resources.InventoryByMonth;
import com.kmorale4.resources.InventoryResource;

import org.jdbi.v3.core.Jdbi;
import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class InventoryApplication extends Application<InventoryConfiguration> {

    public static void main(final String[] args) throws Exception {
        new InventoryApplication().run(args);
    }

    @Override
    public String getName() {
        return "Inventory";
    }

    @Override
    public void initialize(final Bootstrap<InventoryConfiguration> bootstrap) {
    }

    @Override
    public void run(final InventoryConfiguration configuration, final Environment environment) {

        // Connect to DB
        final JdbiFactory orm = new JdbiFactory();
        final Jdbi db = orm.build(environment, configuration.getDataSource(), "mysql");

        // Register Schema with Object Relational Mapper
        db.registerRowMapper(new InventoryMapper());
        db.registerRowMapper(new AuthMapper());

        // Declare controller
        final InventoryResource resource = new InventoryResource(db);
        // Register controller
        environment.jersey().register(resource);

        final InventoryByMonth resourceMonth = new InventoryByMonth(db);
        environment.jersey().register(resourceMonth);

        final InventoryById resourceId = new InventoryById(db);
        environment.jersey().register(resourceId);

        final AuthResource resourceAuth = new AuthResource(db);
        environment.jersey().register(resourceAuth);

        final TemplateHealthCheck healthCheck = new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", healthCheck);

    }

}
