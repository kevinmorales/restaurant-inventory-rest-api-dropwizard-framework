package com.kmorale4.resources;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.kmorale4.db.AuthDAO;
import com.kmorale4.api.User;

import org.jdbi.v3.core.Jdbi;

@Path("/api/register")
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {

    private final AuthDAO authDAO;
    private Utilities utils = new Utilities();

    public AuthResource(Jdbi db) {
        authDAO = db.onDemand(AuthDAO.class);
    }

    @POST
    @Timed
    public Response doPost(String requestBody) {

        String username = "", password = "", newUsername = "", newPassword = "";
        int id = 0;
        int authStatus = 0; // 0 = unauthenticated, 1 = user, 2 = admin

        HashMap<String, String> requestMap = utils.parseJson(requestBody);
        if (requestMap.containsKey("username") && requestMap.containsKey("password")) {
            username = requestMap.get("username");
            password = requestMap.get("password");
        }

        authStatus = checkCredentials(username, password);

        if (authStatus == 2) {

            if (requestMap.containsKey("newUsername") && requestMap.containsKey("newPassword")) {
                newUsername = requestMap.get("newUsername");
                newPassword = requestMap.get("newPassword");

                System.out.println(newUsername);
                System.out.println(newPassword);

                id = register(newUsername, newPassword);

            }

            return Response.ok("{\"id\":" + id + "}").build();

        } else if (authStatus == 1) {

            return Response.status(403).build();

        } else {

            return Response.status(400).build();

        }

    }

    public int register(String username, String password) {

        String passwordString = "";
        boolean success = false;
        int id = -1;
        MessageDigest md;
        byte[] hashedPassword = passwordString.getBytes();
        byte[] salt = new byte[16];
        try {
            // Select the message digest for the hash computation -> SHA-256
            md = MessageDigest.getInstance("SHA-256");

            // Generate the random salt
            SecureRandom random = new SecureRandom();
            salt = new byte[16];
            random.nextBytes(salt);

            // Passing the salt to the digest for the computation
            md.update(salt);

            // Generate the salted hash password
            hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));

            success = true;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if (success) {
            System.out.println("Success");
            // Insert into database
            authDAO.addToDb(username, salt, hashedPassword, "user");
            id = authDAO.getLastId();

            return id;
        } else {
            System.out.println("Failure");
            //

            return id;
        }

    }

    public int checkCredentials(String username, String inputPassword) {

        final String ADMIN = "admin";
        boolean authorized = false;
        MessageDigest md;
        int authStatus = 0;

        User user = authDAO.getUser(username);

        if (user != null) {

            try {
                // Select SHA-256 for hashing
                md = MessageDigest.getInstance("SHA-256");

                // Get salt from user
                byte[] salt = user.salt;

                // Add salt to message digest
                md.update(salt);

                // Generate the salted hash
                byte[] hashedInputPassword = md.digest(inputPassword.getBytes(StandardCharsets.UTF_8));

                System.out.println("Input: " + inputPassword);
                System.out.println("Actual: " + user.password);

                if (Arrays.equals(user.password, hashedInputPassword) && (user.role).equals(ADMIN)) {

                    authStatus = 2;

                } else if (Arrays.equals(user.password, hashedInputPassword))
                    authStatus = 1;
                // END IF

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                authStatus = 0;
            }

        } else
            authStatus = 0;

        return authStatus;
    }

}
