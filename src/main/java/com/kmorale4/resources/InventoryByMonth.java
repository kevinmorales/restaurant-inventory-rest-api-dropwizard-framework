package com.kmorale4.resources;

import com.kmorale4.api.Product;
import com.kmorale4.api.User;
import com.kmorale4.db.AuthDAO;
import com.kmorale4.db.InventoryDAO;
import com.kmorale4.resources.Utilities;
import com.codahale.metrics.annotation.Timed;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import com.codahale.metrics.annotation.Timed;
import org.jdbi.v3.core.Jdbi;
import javax.ws.rs.core.Response;

@Path("/api/inventory/{month}")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InventoryByMonth {

    private final InventoryDAO inventoryDAO;
    private final AuthDAO authDAO;

    private Utilities utils = new Utilities();

    public InventoryByMonth(Jdbi db) {
        inventoryDAO = db.onDemand(InventoryDAO.class);
        authDAO = db.onDemand(AuthDAO.class);
    }

    // ONLY TWO METHODS IN THIS RESOURCE

    // !!! NEEDS FORMATTING !!!
    // Returns all inventory from a given month, specified in path
    @GET
    @Timed
    public Response doGet(@PathParam("month") String month, String requestBody) {

        String username = "";
        String password = "";
        boolean authenticated = false;
        // Get hashmap of json
        HashMap<String, String> requestMap = utils.parseJson(requestBody);

        if (requestMap.containsKey("username") && requestMap.containsKey("password")) {
            username = requestMap.get("username");
            password = requestMap.get("password");
        }
        authenticated = checkCredentials(username, password);

        if (authenticated) {
            month = month.toLowerCase();

            List<Product> monthInventory = inventoryDAO.getByMonth(month);

            if (monthInventory.isEmpty()) {
                return Response.ok("{\"success\": false}").build();
            } else
                return Response.ok(monthInventory).build();
        } else {
            // Unauthenticated
            return Response.status(401).build();
        }

    }

    // !!!NOT DONE!!!
    // Adds to the database for a particuar month, specified in the path
    @POST
    @Timed
    public Response doPost(@PathParam("month") String month, String requestBody) {

        // curl -v -X POST "localhost:8080/inventory/march" -H "Content-Type:
        // application/json" -d '{"name":"baked
        // beans","unit":"lb","unitCost":45.99,"amount":2.1}'
        Product myProduct;
        String name = "";
        String unit = "";
        String username = "";
        String password = "";
        double unitCost = 0;
        double amount = 0;
        double totalCost = 0;
        boolean authenticated = false;
        /*******************************************/

        // Get hashmap of json
        HashMap<String, String> requestMap = utils.parseJson(requestBody);

        if (requestMap.containsKey("username") && requestMap.containsKey("password")) {
            username = requestMap.get("username");
            password = requestMap.get("password");
        }

        authenticated = checkCredentials(username, password);

        if (authenticated) {

            name = requestMap.get("name");
            unit = requestMap.get("unit");
            unitCost = Double.valueOf(requestMap.get("unitCost"));
            amount = Double.valueOf(requestMap.get("amount"));
            totalCost = unitCost * amount;
            myProduct = new Product(name, unit, unitCost, amount, totalCost, month);

            try {
                // Insert product
                inventoryDAO.addToDb(myProduct.name, myProduct.unit, myProduct.unitCost, myProduct.amount,
                        myProduct.totalCost, myProduct.month);

                int id = inventoryDAO.getLastId();

                // Return the id of the new object
                return Response.ok("{\"id\":" + id + "}").build();

            } catch (Exception e) {
                System.out.println(e);
                // Bad Request
                return Response.status(400, "Invalid Request").build();
            }
        } else {
            // Unauthenticated
            return Response.status(401).build();
        }

    }

    public boolean checkCredentials(String username, String inputPassword) {

        boolean authenticated = false;
        MessageDigest md;

        User user = authDAO.getUser(username);

        if (user != null) {

            try {
                // Select SHA-256 for hashing
                md = MessageDigest.getInstance("SHA-256");

                // Get salt from user
                byte[] salt = user.salt;

                // Add salt to message digest
                md.update(salt);

                // Generate the salted hash
                byte[] hashedInputPassword = md.digest(inputPassword.getBytes(StandardCharsets.UTF_8));

                System.out.println("Input: " + inputPassword);
                System.out.println("Actual: " + user.password);

                if (Arrays.equals(user.password, hashedInputPassword))
                    authenticated = true;
                else
                    authenticated = false;
                // END IF

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                authenticated = false;
            }

        } else
            authenticated = false;

        return authenticated;
    }

}