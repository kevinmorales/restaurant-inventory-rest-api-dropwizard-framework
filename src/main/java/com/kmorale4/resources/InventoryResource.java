package com.kmorale4.resources;

import com.kmorale4.api.User;
import com.kmorale4.db.AuthDAO;
import com.kmorale4.db.InventoryDAO;
import com.codahale.metrics.annotation.Timed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import org.jdbi.v3.core.Jdbi;
import javax.ws.rs.core.Response;

@Path("/api/inventory")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryResource {
    private final InventoryDAO inventoryDAO;
    private final AuthDAO authDAO;
    private Utilities utils = new Utilities();

    public InventoryResource(Jdbi db) {
        inventoryDAO = db.onDemand(InventoryDAO.class);
        authDAO = db.onDemand(AuthDAO.class);
    }

    // Returns all inventory
    @GET
    @Timed
    public Response index(String requestBody) {

        String username = "", password = "";
        HashMap<String, String> requestMap = utils.parseJson(requestBody);

        if (requestMap.containsKey("username") && requestMap.containsKey("password")) {
            username = requestMap.get("username");
            password = requestMap.get("password");
        }

        boolean authenticated = checkCredentials(username, password);

        if (authenticated) {
            return Response.ok(inventoryDAO.getAll()).build();

        } else
            return Response.status(401).build();
    }

    public boolean checkCredentials(String username, String inputPassword) {

        boolean authenticated = false;
        MessageDigest md;

        User user = authDAO.getUser(username);

        if (user != null) {

            try {
                // Select SHA-256 for hashing
                md = MessageDigest.getInstance("SHA-256");

                // Get salt from user
                byte[] salt = user.salt;

                // Add salt to message digest
                md.update(salt);

                // Generate the salted hash
                byte[] hashedInputPassword = md.digest(inputPassword.getBytes(StandardCharsets.UTF_8));

                System.out.println("Input: " + inputPassword);
                System.out.println("Actual: " + user.password);

                if (Arrays.equals(user.password, hashedInputPassword))
                    authenticated = true;
                else
                    authenticated = false;
                // END IF

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                authenticated = false;
            }

        } else
            authenticated = false;

        return authenticated;
    }

}