package com.kmorale4.resources;

import com.kmorale4.api.Product;
import com.kmorale4.api.User;
import com.kmorale4.db.AuthDAO;
import com.kmorale4.db.InventoryDAO;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import org.jdbi.v3.core.Jdbi;
import javax.ws.rs.core.Response;

@Path("api//inventory/{month}/{id}")
@Produces(MediaType.APPLICATION_JSON)
public class InventoryById {

    private final InventoryDAO inventoryDAO;
    private final AuthDAO authDAO;
    private Utilities utils = new Utilities();

    public InventoryById(Jdbi db) {
        inventoryDAO = db.onDemand(InventoryDAO.class);
        authDAO = db.onDemand(AuthDAO.class);
    }

    // Returns a product with a given id the db
    @GET
    @Timed
    public Response doGet(@PathParam("month") String month, @PathParam("id") Integer id, String requestBody) {

        String username = "";
        String password = "";
        boolean authenticated = false;
        // Get hashmap of json
        HashMap<String, String> requestMap = utils.parseJson(requestBody);

        if (requestMap.containsKey("username") && requestMap.containsKey("password")) {
            username = requestMap.get("username");
            password = requestMap.get("password");
        }
        authenticated = checkCredentials(username, password);

        if (authenticated) {

            Product product = inventoryDAO.getByMonthAndId(id, month);

            if (product != null) {
                return Response.ok(product).build();
            } else {
                return Response.status(400, "Invalid Path Parameters").build();
            }
        } else {
            return Response.status(401).build();
        }
    }

    // Edits a product with given request body and id
    @PUT
    @Timed
    public Response doPut(@PathParam("month") String month, @PathParam("id") String id, String requestBody) {

        String name = "";
        String unit = "";
        String username = "";
        String password = "";
        double unitCost = 0;
        double amount = 0;
        double totalCost = 0;
        boolean authenticated = false;
        /*********************************/

        HashMap<String, String> requestMap = utils.parseJson(requestBody);

        if (requestMap.containsKey("username") && requestMap.containsKey("password")) {
            username = requestMap.get("username");
            password = requestMap.get("password");
        }

        authenticated = checkCredentials(username, password);

        if (authenticated) {

            if (requestMap.containsKey("name") && requestMap.containsKey("unit") && requestMap.containsKey("unitCost")
                    && requestMap.containsKey("amount")) {

                name = requestMap.get("name");
                unit = requestMap.get("unit");
                unitCost = Double.valueOf(requestMap.get("unitCost"));
                amount = Double.valueOf(requestMap.get("amount"));
                totalCost = amount * unitCost;

                inventoryDAO.updateInventory(Integer.valueOf(id), name, unit, unitCost, amount, totalCost, month);
                return Response.status(200).build();

            } else {
                return Response.status(400).build();
            }
        } else {
            return Response.status(401).build();
        }

    }

    // Deletes a product with a given id
    @DELETE
    @Timed
    public Response doDelete(@PathParam("month") String month, @PathParam("id") Integer id, String requestBody) {

        String username = "", password = "";
        HashMap<String, String> requestMap = utils.parseJson(requestBody);

        if (requestMap.containsKey("username") && requestMap.containsKey("password")) {
            username = requestMap.get("username");
            password = requestMap.get("password");
        }

        boolean authenticated = checkCredentials(username, password);

        if (authenticated) {

            Product product = inventoryDAO.getByMonthAndId(id, month);

            if (product != null) {

                inventoryDAO.deleteInventory(id, month);

                return Response.ok(product).build();
            } else {
                return Response.status(400, "Invalid Path Parameters").build();
            }
        } else
            return Response.status(401).build();

    }

    public boolean checkCredentials(String username, String inputPassword) {

        boolean authenticated = false;
        MessageDigest md;

        User user = authDAO.getUser(username);

        if (user != null) {

            try {
                // Select SHA-256 for hashing
                md = MessageDigest.getInstance("SHA-256");

                // Get salt from user
                byte[] salt = user.salt;

                // Add salt to message digest
                md.update(salt);

                // Generate the salted hash
                byte[] hashedInputPassword = md.digest(inputPassword.getBytes(StandardCharsets.UTF_8));

                if (Arrays.equals(user.password, hashedInputPassword))
                    authenticated = true;
                else
                    authenticated = false;
                // END IF

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                authenticated = false;
            }

        } else
            authenticated = false;

        return authenticated;
    }

}
