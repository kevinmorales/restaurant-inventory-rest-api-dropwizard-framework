package com.kmorale4.resources;

import java.util.HashMap;

import com.kmorale4.api.Product;

public class Utilities {

    public Utilities() {
        super();
    }

    public HashMap<String, String> parseJson(String json) {

        // local constants

        // local variables
        HashMap<String, String> myMap = new HashMap<String, String>();
        String[] jsonSplitArray;

        /************************/

        // Parse json into comma separated values
        json = json.replace("\'", "").replace("}", "").replace("\"", "").replace("{", "");

        System.out.println(json);

        // Split on the commas
        jsonSplitArray = json.split(",");

        for (String x : jsonSplitArray)
            System.out.println(x);

        for (int i = 0; i < jsonSplitArray.length; i++) {

            String key = jsonSplitArray[i].split(":")[0];
            String value = jsonSplitArray[i].split(":")[1];

            System.out.println("Key: " + key);
            System.out.println("Value: " + value);

            myMap.put(key, value);

        } // END FOR

        return myMap;

    }

    public Product parseJson(String json, String month) {

        // local constants

        // local variables
        HashMap<String, String> requestMap = new HashMap<String, String>();
        String[] jsonSplitArray;
        Product myProduct;
        String name, unit;
        double unitCost, amount, totalCost;

        /************************/

        // Parse json into comma separated values
        json = json.replace("\'", "").replace("}", "").replace("\"", "").replace("{", "");

        System.out.println(json);

        // Split on the commas
        jsonSplitArray = json.split(",");

        for (String x : jsonSplitArray)
            System.out.println(x);

        for (int i = 0; i < jsonSplitArray.length; i++) {

            String key = jsonSplitArray[i].split(":")[0];
            String value = jsonSplitArray[i].split(":")[1];

            System.out.println("Key: " + key);
            System.out.println("Value: " + value);

            requestMap.put(key, value);

        } // END FOR

        // Get Product data from json request body
        name = requestMap.get("name");
        unit = requestMap.get("unit");
        unitCost = Double.valueOf(requestMap.get("unitCost"));
        amount = Double.valueOf(requestMap.get("amount"));
        totalCost = unitCost * amount;
        myProduct = new Product(name, unit, unitCost, amount, totalCost, month);
        return myProduct;

    }

    public String[] parseJson(String json, boolean authorized) {

        // local constants

        // local variables
        HashMap<String, String> myMap = new HashMap<String, String>();
        String[] jsonSplitArray;
        String[] credentials = new String[2];

        /************************/

        // Parse json into comma separated values
        json = json.replace("\'", "").replace("}", "").replace("\"", "").replace("{", "");

        System.out.println(json);

        // Split on the commas
        jsonSplitArray = json.split(",");

        for (String x : jsonSplitArray)
            System.out.println(x);

        for (int i = 0; i < jsonSplitArray.length; i++) {

            String key = jsonSplitArray[i].split(":")[0];
            String value = jsonSplitArray[i].split(":")[1];

            System.out.println("Key: " + key);
            System.out.println("Value: " + value);

            myMap.put(key, value);

        } // END FOR

        if (myMap.containsKey("username") && myMap.containsKey("password")) {
            credentials[0] = myMap.get("username");
            credentials[1] = myMap.get("password");

        } else {
            credentials[0] = "-1";
            credentials[1] = "-1";
        }

        return credentials;
    }

}
