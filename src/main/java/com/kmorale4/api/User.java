package com.kmorale4.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "users")
public class User {

    @Column(name = "name")
    @JsonProperty
    public String name;

    @Column(name = "salt")
    @JsonProperty
    public byte[] salt;

    @Column(name = "user_password")
    @JsonProperty
    public byte[] password;

    @Column(name = "role")
    @JsonProperty
    public String role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @JsonProperty
    public int id;

    public User() {
        super();
    }

    public User(String name, byte[] salt, byte[] password, String role) {
        this.name = name;
        this.salt = salt;
        this.password = password;
        this.role = role;
    }

    public User(int id, String name, byte[] salt, byte[] password, String role) {
        this.id = id;
        this.name = name;
        this.salt = salt;
        this.password = password;
        this.role = role;
    }
}