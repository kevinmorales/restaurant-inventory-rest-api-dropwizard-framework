package com.kmorale4.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "product")
public class Product {

    @Column(name = "name")
    @JsonProperty
    public String name;

    @Column(name = "unit")
    @JsonProperty
    public String unit;

    @Column(name = "month")
    @JsonProperty
    public String month;

    @Column(name = "unit_cost")
    @JsonProperty
    public double unitCost;

    @Column(name = "amount")
    @JsonProperty
    public double amount;

    @Column(name = "total_cost")
    @JsonProperty
    public double totalCost;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @JsonProperty
    public int id;

    public Product() {
        super();
    }

    public Product(int id, String name, String unit, double unitCost, double amount, double totalCost) {
        this.name = name;
        this.unit = unit;
        this.unitCost = unitCost;
        this.amount = amount;
        this.totalCost = totalCost;
        this.id = id;

    }

    public Product(String name, String unit, double unitCost, double amount, double totalCost, String month) {
        this.name = name;
        this.unit = unit;
        this.unitCost = unitCost;
        this.amount = amount;
        this.totalCost = totalCost;
        this.month = month;

    }

    public Product(int id, String name, String unit, double unitCost, double amount, double totalCost, String month) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.unitCost = unitCost;
        this.amount = amount;
        this.totalCost = totalCost;
        this.month = month;

    }

}
